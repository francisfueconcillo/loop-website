import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Switch } from "react-router-dom";
import ReactGA from 'react-ga';

import { ThemeProvider } from '@material-ui/core/styles';
import { LoopTheme } from './theme';

import { AppConfig, AppHistory } from "./AppConfig";
import App from './App';
import { PrivacyPage } from './pages';
import './index.css';

ReactGA.initialize(AppConfig.GA_TRACKING_ID);

AppHistory.listen(location => {
  ReactGA.set({ page: location.pathname + location.hash });
  ReactGA.pageview(location.pathname + location.hash);
});

ReactDOM.render(
  <ThemeProvider theme={LoopTheme}>
    <Router history={AppHistory}>
      <Switch>
        <Route path="/privacy" component={PrivacyPage} />
        <Route path="/" component={App} />
      </Switch>
    </Router>
  </ThemeProvider>,
  document.getElementById("root")
);