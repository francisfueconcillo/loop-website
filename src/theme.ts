import { createMuiTheme } from '@material-ui/core/styles';

export const palette = {
  COLOR1: '#4FC2F8',
}

export const LoopTheme = createMuiTheme({
  palette: {
    primary: {
      main: palette.COLOR1,
    },
  },
});