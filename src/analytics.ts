/**
 * Event - Add custom tracking event.
 * @param {string} category 
 * @param {string} action 
 * @param {string} label 
 */

import ReactGA from 'react-ga';

export const Event = (category:string, action:string, label:string) => {
  ReactGA.event({
    category: category,
    action: action,
    label: label
  });
};