import React from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

export const PrivacyPage = () => {
  return (
    <React.Fragment>
      <CssBaseline />

      <Container maxWidth="md">

        <h2>Privacy Policy</h2>
        
        <h4>Effectivity Date: June 1, 2020</h4>

        <p>
          This Privacy Policy explains the privacy practices of Systems and Shelves Inc. 
          with respect to the information collected through the Loop application. 
        </p>

        <h3>Information we collect</h3>
        <p>
          We may collect and store the following information that you provide directly or generate through your use of Loop:
        </p>

        <ul>
          <li>
            contact information (such as email address, postal code, country);
          </li>
          <li>
            information about your use of Loop (such as messages sent or received in Loop; 
            Loop settings; and in-app usage information, including menus and settings clicked, 
            features used, frequency and duration of use of the app, and any error messages displayed);
          </li>
          <li>
            Loop profile data (such as user name, date of birth, gender, profile photo, postings);
          </li>
          <li>Apple, Google and/or Facebook account information (such as Account IDs, Email);</li>
          <li>
            device information (such as IP address; firmware version; operating system; 
            and device identifiers, including MAC address, Android ID, serial number, and 
            mobile advertiser identifiers);
          </li>
          <li>
            geolocation data, where location-based features and location settings have been 
            activated on your device. (With your permission, we may receive your device’s precise 
            geolocation and other information related to your location through GPS, Bluetooth, and 
            Wi-Fi signals, and other technologies for certain purposes, such as to provide you with 
            the Loop services. Your device will notify you when the Loop services attempt to collect 
            your precise geolocation data. Please note that if you decline to allow Loop to collect 
            your precise geolocation, you may not be able to use all of the Loop services or features.);
          </li>

          <li>content you post, send, or receive through Loop’s social network feature, 
            where users can communicate with other Loop users and share content (including posts, 
            messages, replies, likes, and comments).
          </li>

        </ul>

        <h3>How we use the information we collect</h3>
        <p>
          We may use this information:
        </p>

        <ul>
          <li>
            to respond to your requests and inquiries;
          </li>
          <li>
            to communicate with you about Loop;
          </li>
          <li>
            to provide services or features that you request, including data back-up and sync 
            services for Loop account users;
          </li>
          <li>
            for advertising, such as providing customized advertisements, sponsored content, and 
            sending you promotional communications;
          </li>
          <li>
            to deliver customized content and recommendations tailored to your interests and the 
            manner in which you interact with Loop;
          </li>
          <li>
            to operate, evaluate, and improve our business (including developing new products; 
            enhancing and improving our products and Loop; managing our communications; analyzing our 
            products, consumer base, and Loop; performing data analytics; and performing accounting, 
            auditing, and other internal functions);
          </li>
          <li> 
            to protect against, identify, and prevent fraud and other criminal activity, claims, 
            and other liabilities; and
          </li> 

          <li>
            to comply with and enforce applicable legal requirements, relevant industry standards 
            and our policies, including this Privacy Policy
          </li>

        </ul>

        <p>
          We may combine information we obtain about you from Loop with information collected about 
          you through our other services for the purposes described above. We also may collect other 
          information about you, your devices and apps, and your use of Loop, in ways that we describe 
          to you at the time we collect it or otherwise with your consent.
        </p>

        <h3>Information sharing</h3>
        <p>
          When you use Loop with third party applications, you may choose to share or synchronize your 
          information with such services and applications. We will grant access to these other services 
          and third party applications only when you authorize us to do so. We will not disclose your 
          personal information to third parties for their own independent marketing or business purposes
          without your consent. We may share your personal information with our subsidiaries and 
          affiliates and with third party service providers as necessary, for the purposes of providing 
          the Loop services and features as described above.
        </p>

        <p>
          We, our subsidiaries and affiliates, and our third party service providers also may disclose 
          information about you (1) if we are required to do so by law or legal proceedings (such as a 
          court order or subpoena); (2) in response to requests by government agencies, such as law 
          enforcement authorities; (3) to establish, exercise, or defend our legal rights; (4) when 
          we believe disclosure is necessary or appropriate to prevent physical or other harm or 
          financial loss; (5) in connection with an investigation of suspected or actual illegal activity; 
          (6) in the event of a potential or actual sale or transfer of all or a portion of our business or 
          assets (including in the event of a merger, acquisition, joint venture, reorganization, 
          divestiture, dissolution, or liquidation); or (7) otherwise with your consent.
        </p>

      <h3>Links to third-party devices, apps and features</h3>

      <p>
        Loop may provide the ability to connect to third party devices, apps, and features. 
        These devices, apps, and features may operate independently from us and may have their own 
        privacy policies, which we strongly suggest you review. To the extent any linked third party 
        device, app, or feature is not owned or controlled by us, we are not responsible for its content,
        any use of the device, app, or feature, or the privacy practices of the operator of the device, 
        app, or feature.
      </p>

      <h3>Your choices</h3>

      <p>
        Please visit the Loop settings on your device for choices that may be available to you in 
        connection with your use of Loop. Loop offers choices related to sharing certain information 
        with Loop and receiving certain marketing information and communications about products, 
        services, and promotions from Loop. Please note that if you decline to allow Loop to collect 
        or share certain information, you may not be able to use all of the features available through 
        Loop. When you delete the Loop application, the information saved to your device will be 
        deleted but any information collected and stored by us will still remain on our Loop server. 
        If you want to delete the information collected through Loop from both your device and our 
        Loop server, you can do so by contacting us requesting to erase your personal data. 
        However, any third party applications with which you have authorized us to share or synchronize 
        your information may continue to store your information subject to their own privacy policies 
        and practices.
      </p>

      <h3>How we protect personal information</h3>
      <p>
        We maintain administrative, technical, and physical safeguards designed to protect personal 
        information we obtain through Loop against accidental, unlawful, or unauthorized destruction, 
        loss, alteration, access, disclosure, or use.
      </p>

      <h3>Updates to this Privacy Policy</h3>
      <p>
        This Privacy Policy may be updated periodically and without prior notice to you to reflect 
        changes in our personal information practices with respect to Loop. We will indicate at the 
        top of the notice when it was most recently updated.
      </p>

      </Container>
    </React.Fragment>
  );
};