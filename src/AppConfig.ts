import { createBrowserHistory } from "history";

export const AppConfig = {
    APP_TITLE: 'Loop',
    GA_TRACKING_ID: 'UA-169323937-1',   
}
    
export const AppHistory = createBrowserHistory();