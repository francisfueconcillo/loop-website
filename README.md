# Loop -Website

## Technologies used
- React with Typescript
- Material UI

## Local development setup
- Clone this repository
- Install node dependencies
```
$ cd loop-website
$ npm i
```
- Run locally
```
npm start
```
- Website will be running at http://localhost:3000

## Deployment
- Build for production. The following command will create /build directory
```
$ npm run build
```
- Website is hosted on Google Cloud - App Engine
- Deployment needs access credentials. 
- [TO-DO] - Setup Bitbucket CI/CD to deploy to GCP

